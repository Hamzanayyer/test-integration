import React from "react"
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "./index.css"
import { BrowserRouter as Router, Route } from "react-router-dom"
import Details from "./../components/Details/Details"
import Home from "./../components/Home/Home"
import data from "../data/items.json"
import { AnimatedSwitch } from "react-router-transition"

const IndexPage = () => (
  <Router>
    <AnimatedSwitch
      atEnter={{ opacity: 0 }}
      atLeave={{ opacity: 0 }}
      atActive={{ opacity: 1 }}
      className="switch-wrapper"
    >
      <Route exact path="/" component={() => <Home data={data} />} />
      {data.map((item, index) => {
        return (
          <Route
            key={index}
            path={item.path}
            component={() => <Details data={item} />}
          />
        )
      })}
    </AnimatedSwitch>
  </Router>
)
export default IndexPage
