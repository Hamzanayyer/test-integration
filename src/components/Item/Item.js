import React from "react"
import "./Item.css"
import { Link } from "react-router-dom"

const Item = ({ item }) => {
  return (
    <div
      className="d-flex flex-column justify-content-end item-bloc"
      style={{ backgroundColor: item.backgroundColor }}
    >
      <Link to={item.path}>
        <img
          className="d-block mx-auto item-link"
          src={item.image}
          alt="item"
        />
        <div className="text-details mt-4">
          <h4>{item.name}</h4>
          <h5 style={{ color: item.priceColor }}>${item.price}</h5>
        </div>
      </Link>
    </div>
  )
}

export default Item
