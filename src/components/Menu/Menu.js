import React from "react"
import "./Menu.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCircle } from "@fortawesome/free-solid-svg-icons"

const Menu = () => {
  const [currentActive, setCurrentActive] = React.useState(0)
  const [item] = React.useState(["Chairs", "Sofas", "Tables", "Lamps"])

  return (
    <ul className="d-flex topNav">
      {item.map((data, index) => {
        return (
          <li key={index}>
            <p
              className={`${currentActive === index ? "active" : "deactive"}`}
              onClick={() => setCurrentActive(index)}
            >
              {data}
            </p>
            {currentActive === index && (
              <div className="dot">
                <FontAwesomeIcon
                  style={{
                    color: "black",
                    width: "5px",
                    height: "5px",
                  }}
                  icon={faCircle}
                />
              </div>
            )}
          </li>
        )
      })}
    </ul>
  )
}

export default Menu
