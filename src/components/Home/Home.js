import React from "react"
import "./Home.css"
import Item from "./../Item/Item"
import Menu from "./../Menu/Menu"
const Home = ({ data }) => {
  return (
    <div className="container index-content">
      <div className="row row-box">
        <h1>What item are you looking for?</h1>
        <Menu className="nav-menu" />
        <div className="row d-flex justify-content-center item-element">
          {data.map((item, index) => {
            return (
              <div key={index} className="col-5 d-flex justify-content-center">
                <Item item={item} />
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default Home
