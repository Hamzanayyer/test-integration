import React from "react"
import "./Details.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faCircle,
  faShoppingBasket,
  faArrowLeft,
} from "@fortawesome/free-solid-svg-icons"
import { Link } from "react-router-dom"

const Details = ({ data }) => {
  const [qty, setQty] = React.useState(1)
  const [currentActive, setCurrentActive] = React.useState(0)

  const colors = ["#171c23", "#fc9090", "#8ed7a6"]

  return (
    <div className="container-fluid details-page">
      <div
        style={{
          backgroundColor: data.backgroundDetails,
          borderRadius: "16px 16px 0 0",
        }}
        className="row details"
      >
        <div className="col-12 image-block">
          <div className="row">
            <div className="col">
              <Link to="/">
                <div className="arrow-left d-flex justify-content-center">
                  <FontAwesomeIcon
                    style={{
                      color: data.symbolColor,
                      paddingTop: "13px",
                      paddingRight: "1px",
                    }}
                    size={"2x"}
                    icon={faArrowLeft}
                    className="arrow-icon"
                  />
                </div>
              </Link>
            </div>
          </div>
          <img
            className="d-block mx-auto img-item"
            style={{ height: "234px", width: "246px" }}
            src={data.imageDetails}
            alt="item"
          />
        </div>
        <div
          style={{ borderRadius: "16px 16px 0 0" }}
          className="col-12 border-white"
        >
          <div className="row">
            <div className="col d-flex justify-content-between">
              <h3 className="item-title">{data.title}</h3>
              <h3 style={{ color: data.priceColor }} className="item-price">
                ${data.price}
              </h3>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <h5 className="item-type">{data.type}</h5>
              <p className="item-description">{data.description}</p>
            </div>
          </div>

          <div className="row cart-qty">
            <div className="col d-flex justify-content-between">
              <div className="d-flex">
                {colors.map((color, index) => {
                  return (
                    <div
                      key={index}
                      className={`${
                        currentActive === index ? "activeColor" : ""
                      }`}
                      style={{
                        backgroundColor: color,
                        width: "28px",
                        height: "28px",
                        marginRight: "17px",
                        borderRadius: "50%",
                      }}
                      icon={faCircle}
                      onClick={() => setCurrentActive(index)}
                    ></div>
                  )
                })}
              </div>
              <div className="d-flex">
                <div
                  onClick={() => setQty(qty > 1 ? qty - 1 : 1)}
                  className="d-flex justify-content-center remove-btn"
                  style={{ backgroundColor: data.backgroundDetails }}
                >
                  <p style={{ color: data.symbolColor }}>-</p>
                </div>
                <div className="qty">{qty}</div>
                <div
                  onClick={() => setQty(qty >= 10 ? qty : qty + 1)}
                  className="d-flex justify-content-center add-btn"
                  style={{ backgroundColor: data.backgroundDetails }}
                >
                  <p style={{ color: data.symbolColor }}>+</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div
          className="col-12 btn-bottom d-flex justify-content-center align-items-center"
          style={{
            backgroundColor: data.backgroundDetails,
            borderRadius: "16px 16px 0 0",
          }}
          fixed="bottom"
        >
          <div>
            <p
              onClick={() =>
                console.log(
                  "%cProduit ajouté au panier",
                  "color: green; font-weight: bold"
                )
              }
              className="btn-cart"
              style={{
                color: data.textColor,
              }}
            >
              <span>
                <FontAwesomeIcon
                  className="cart"
                  style={{
                    color: data.textColor,
                    width: "21px",
                    height: "18px",
                    marginRight: "18px",
                    paddingTop: "2px",
                  }}
                  icon={faShoppingBasket}
                />
              </span>
              Add to Cart
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Details
